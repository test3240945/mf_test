/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.example.mf_test;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "com.example.mf_test";
  public static final String BUILD_TYPE = "debug";
}
